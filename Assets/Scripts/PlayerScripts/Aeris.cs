﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Aeris : Avatar {


	public int mBarrierPercent, pBarrierPercent, limitPercent;

	public Aeris() {
		characterName = "Aeris";
		maxHealth = 283;
		currentHealth = maxHealth;
		maxMP = 73;
		currentMP = maxMP;
		mBarrierPercent = 0;
		pBarrierPercent = 0;
		limitPercent = 0;
		str = 16;
		vit = 20;
		mag = 25;
		spr = 19;
		dex = 4;
		lck = 11;

	}

	/*private List<Materia> aerisCommands = new List<Materia>();
	private List<Materia> aerisMagic = new List<Materia>();
	private List<Materia> aerisItems = new List<items>();
	private List<summons> aerisSummons = new List<summons>();
	//private List<enemySkills> aerisEnemySkills = new List<enemySkills>;

	void Awake(){
		aerisCommands.Add(commands.Attack);
		aerisMagic.Add(magic.Lightning);
		aerisMagic.Add(magic.Heal);
		aerisSummons.Add(summons.Shiva);
		aerisItems.Add(items.Potion);
		aerisItems.Add(items.Ether);
	}*/






	bool setHealth(int h)
	{
		if (h <= 0) 
		{

			Destroy(this.gameObject); 
			return false;
		}


		if (h > this.maxHealth) 
		{
			this.currentHealth = this.maxHealth;
		}
		else 
		{
			this.currentHealth = h;
		}

		return true;

	}

	public bool modifyHealth(int howMuch)
	{
		return (this.setHealth(this.currentHealth + howMuch));

	}

	public int getHealth()
	{
		return this.currentHealth;
	}

	public int getMaxHealth()
	{
		return this.maxHealth;
	}


}
