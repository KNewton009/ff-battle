﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleController : MonoBehaviour {
	public List<BattleData> battleDataList;

	public enum BattleStates
	{
		Init,
		ChooseAction,
		ResolveAction,
		Victory,
		Defeat,
	}

	//SoundClips for battle states
	public AudioClip fanfare;
	public AudioClip defeat;

	public List<GameObject> playerChars;
	public List<GameObject> npcEnemies;
	public List<GameObject> turnOrder = new List<GameObject>();
	public BattleStates currentBattleState;

	public FiniteStateMachine<BattleController, BattleStates> battleFSM;

	//Instantiate and Intialize new battle states
	private InitState bInitState = new InitState();
	private ChooseState bChooseState = new ChooseState();
	private ResolveState bResolveState = new ResolveState();
	private VictoryState bVictoryState = new VictoryState();
	private DefeatState bDefeatState = new DefeatState();



	// Use this for initialization
	void Start () {

		battleDataList = new List<BattleData>();

		battleFSM = new FiniteStateMachine<BattleController, BattleStates>(this);
		battleFSM.RegisterState(bInitState);
		battleFSM.RegisterState(bChooseState);
		battleFSM.RegisterState(bResolveState);
		battleFSM.RegisterState(bVictoryState);
		battleFSM.RegisterState(bDefeatState);

		// Start in init
		this.ChangeState(BattleStates.Init);


	}

	public void UpdatePlayerBattleData(List<BattleData> playerBattleDataList)
	{
		// Add the player choices to the list that should already have the NPCs by now
		foreach(BattleData data in playerBattleDataList)
		{
			battleDataList.Add(data);
		}
		// Now that we have the player battle data, we can resolve this round
		this.ChangeState(BattleStates.ResolveAction);
	}

	public void ChangeState(BattleStates newState)
	{
		battleFSM.ChangeState(newState);
		currentBattleState = newState;
	}

	void Update () 
	{
		battleFSM.Update();
	}



}
