﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Player : Avatar {
	
	public int mBarrierPercent, pBarrierPercent, limitPercent;


	public Player() {
		
		maxHealth = 314;
		currentHealth = maxHealth;
		maxMP = 54;
		currentMP = maxMP;
		mBarrierPercent = 0;
		pBarrierPercent = 0;
		limitPercent = 0;
		str = 20;
		vit = 16;
		mag = 19;
		spr = 17;
		dex = 6;
		lck = 14;

	}
		

	void awake(){
		name = "Cloud";
	}





	// Call this when you want to attack the enemy
	void OnCollisionEnter (Collision collision)
	{
		if( collision.gameObject.tag == "Enemy" )
		{
			Invoke ("EnterBattle", 0f);
			//Battle Start Animation
		}
	}

	void EnterBattle()
	{
		
		SceneManager.LoadScene("RPG Battle");

	}



	bool setHealth(int h)
	{
		if (h <= 0) 
		{

			Destroy(this.gameObject); 
			return false;
		}


		if (h > this.maxHealth) 
		{
			this.currentHealth = this.maxHealth;
		}
		else 
		{
			this.currentHealth = h;
		}

		return true;

	}

	public bool modifyHealth(int howMuch)
	{
		return (this.setHealth(this.currentHealth + howMuch));

	}

	public int getHealth()
	{
		return this.currentHealth;
	}

	public int getMaxHealth()
	{
		return this.maxHealth;
	}




}
