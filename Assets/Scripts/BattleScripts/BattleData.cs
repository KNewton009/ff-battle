﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleData : MonoBehaviour {

	public Player attacker;
	public Enemy target;
	public Actions choice;

	public string mName, mProperty;
	public int minDamage, maxDamage;

	public enum Actions{

		Attack,
		Fire,
		Poison,
		Heal,
		Lightning,
		Shiva


	};

	public Dictionary<Actions, int> maxValue = new Dictionary<Actions, int> ()
	{
		{Actions.Attack, -10},
		{Actions.Fire, -10},
		{Actions.Poison, -10},
		{Actions.Heal, -8},
		{Actions.Lightning, +10},
		{Actions.Shiva, -10},
	};


	public Dictionary<Actions, int> minValue = new Dictionary<Actions, int> ()
	{
		{Actions.Attack, -10},
		{Actions.Fire, -10},
		{Actions.Poison, -10},
		{Actions.Heal, -8},
		{Actions.Lightning, +10},
		{Actions.Shiva, -10},
	};

	public BattleData(Player attacker, Enemy target, Actions choice)
	{
		this.attacker = attacker;
		this.target = target;
		this.choice = choice;
	}
		
	}





