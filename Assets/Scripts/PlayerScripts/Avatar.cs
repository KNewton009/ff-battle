﻿using UnityEngine;
using System.Collections;

public class Avatar : MonoBehaviour {

	public int currentHealth, maxHealth, currentMP, maxMP; 
	public string characterName;

	//Base stats for battle
	public int str, vit, spr, dex, mag, lck;
	public int att, def, magAtt, magDefPercent, attPercent, defPercent, magDef;


	public Avatar() {
		currentHealth = maxHealth;
		maxHealth = 0;
		currentMP = maxMP;
		maxMP = 0;
		str = 0;
		vit = 0;
		spr = 0;
		dex = 0;
		lck = 0;
		att = 0;
		def = 0;
		magAtt = 0;
		magDef = 0;
		magDefPercent = 0;
		attPercent = 0;
		defPercent = 0;
		magDef = 0;

	}
			
}
