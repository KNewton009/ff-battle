﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class CanvasScript : MonoBehaviour {

	private GameObject characterOne, characterTwo, characterThree;
	private GameObject charOneBattlePanel, charTwoBattlePanel;
	private GameObject charOneMagicPanel, charOneSummonPanel, charOneItemPanel;
	private GameObject charTwoMagicPanel, charTwoSummonPanel, charTwoItemPanel;
	public AudioClip confirmSound;
	public AudioSource confirmSoundSource;
	private List<BattleData>bdList = new List<BattleData>();
	private GameObject battleController;
	private int currentPlayerIndex = 0;
	private Enemy enemy;


	//Instantiate Character 1 GameObjects
	private GameObject charOneName, charOneMBarrier, charOnePBarrier, charOneCurrentHP, charOneMaxHP, charOneHPSlider,
					  charOneMP, charOneMPSlider, charOneLimit,
					 charOneMagicOption1, charOneMagicOption2, charOneMagicOption3, charOneItemsOption1, charOneItemsOption2;

	//Instantiate Character 2 GameObjects
	private GameObject charTwoName, charTwoMBarrier, charTwoPBarrier, charTwoCurrentHP, charTwoMaxHP, charTwoHPSlider,
					  charTwoMP, charTwoMPSlider, charTwoLimit,
					charTwoMagicOption1, charTwoMagicOption2, charTwoSummonOption1, charTwoItemsOption1, charTwoItemsOption2;

	//Instantiate Character 3 GameObjects
	/*private GameObject charThreeName, charThreeMBarrier, charThreePBarrier, charThreeCurrentHP, charThreeMaxHP, charThreeHPSlider,
					  charThreeMP, charThreeMPSlider, charThreeLimit, charThreeWait,
					charThreeOption1, charThreeOption2, charThreeOption3, charThreeOption4;
*/

	// Use this for initialization
	void Start () {

		battleController = GameObject.Find("Battle Controller");
		charOneBattlePanel = GameObject.Find("Char1 Battle Panel");
		charOneMagicPanel = GameObject.Find("Char1 Magic Panel");
		charOneSummonPanel = GameObject.Find("Char1 Summon Panel");
		charOneItemPanel = GameObject.Find("Char1 Item Panel");

		charTwoBattlePanel = GameObject.Find("Char2 Battle Panel");
		charTwoMagicPanel = GameObject.Find("Char2 Magic Panel");
		charTwoSummonPanel = GameObject.Find("Char2 Summon Panel");
		charTwoItemPanel = GameObject.Find("Char2 Item Panel");

		//Initialize character 1 GameObjects
		characterOne = GameObject.Find("Character1");
		charOneName = GameObject.Find("Char1 Name");
		charOneMBarrier = GameObject.Find("Char1 BarrierU");
		charOnePBarrier = GameObject.Find("Char1 BarrierD");
		charOneCurrentHP = GameObject.Find("Char1 Current HP");
		charOneHPSlider = GameObject.Find("Char1 HP Slider");
		charOneMaxHP = GameObject.Find("Char1 Max HP");
		charOneMPSlider = GameObject.Find("Char1 MP Slider");
		charOneMP = GameObject.Find("Char1 MP");
		charOneLimit = GameObject.Find("Char1 Limit Slider");
		charOneMagicOption1 = GameObject.Find("Char1 Magic Option1");
		charOneMagicOption2 = GameObject.Find("Char1 Magic Option2");
		charOneMagicOption3 = GameObject.Find("Char1 Magic Option3");
		charOneItemsOption1 = GameObject.Find("Char1 Item Option1");
		charOneItemsOption2 = GameObject.Find("Char1 Item Option2");

		//Initialize character 2 GameObjects
		characterTwo = GameObject.Find("Character2");
		charTwoName = GameObject.Find("Char2 Name");
		charTwoMBarrier = GameObject.Find("Char2 BarrierU");
		charTwoPBarrier = GameObject.Find("Char2 BarrierD");
		charTwoCurrentHP = GameObject.Find("Char2 Current HP");
		charTwoHPSlider = GameObject.Find("Char2 HP Slider");
		charTwoMaxHP = GameObject.Find("Char2 Max HP");
		charTwoMP = GameObject.Find("Char2 MP");
		charTwoMPSlider = GameObject.Find("Char2 MP Slider");
		charTwoLimit = GameObject.Find("Char2 Limit Slider");
		charTwoMagicOption1 = GameObject.Find("Char2 Magic Option1");
		charTwoMagicOption2 = GameObject.Find("Char2 Magic Option2");
		charTwoSummonOption1 = GameObject.Find("Char2 Summon Option1");
		charTwoItemsOption1 = GameObject.Find("Char2 Item Option1");
		charTwoItemsOption2 = GameObject.Find("Char2 Item Option2");

		/*//Initialize character 3 GameObjects
		characterThree = GameObject.Find("Character3");
		charThreeName = GameObject.Find("Char3 Name");
		charThreeMBarrier = GameObject.Find("Char3 BarrierU");
		charThreePBarrier = GameObject.Find("Char3 BarrierD");
		charThreeCurrentHP = GameObject.Find("Char3 Current HP");
		charThreeHPSlider = GameObject.Find("Char3 HP Slider");
		charThreeMaxHP = GameObject.Find("Char3 Max HP");
		charThreeMP = GameObject.Find("Char3 MP");
		charThreeMPSlider = GameObject.Find("Char3 MP Slider");
		charThreeLimit = GameObject.Find("Char3 Limit Slider");
		charThreeWait = GameObject.Find("Char3 Time Slider");
		charThreeOption1 = GameObject.Find("Char3 Attack");
		charThreeOption2 = GameObject.Find("Char3 Option2");
		charThreeOption4 = GameObject.Find("Char3 Option4");*/


		setPlayerOne();
		setPlayerTwo();

		/*sforeach(Player chars in battleController )*/

	}
	


	void setPlayerOne(){


		charOneName.GetComponent<Text>().text = characterOne.GetComponent<Player>().characterName;
		charOneCurrentHP.GetComponent<Text>().text = characterOne.GetComponent<Player>().currentHealth.ToString();
		charOneMaxHP.GetComponent<Text>().text = characterOne.GetComponent<Player>().maxHealth.ToString();
		charOneMP.GetComponent<Text>().text = characterOne.GetComponent<Player>().currentMP.ToString();
		charOneMBarrier.GetComponent<Slider>().value = characterOne.GetComponent<Player>().mBarrierPercent;
		charOnePBarrier.GetComponent<Slider>().value = characterOne.GetComponent<Player>().pBarrierPercent;
		charOneLimit.GetComponent<Slider>().value = characterOne.GetComponent<Player>().limitPercent;


		charOneHPSlider.GetComponent<Slider>().maxValue = characterOne.GetComponent<Player>().maxHealth;
		charOneHPSlider.GetComponent<Slider>().value = characterOne.GetComponent<Player>().currentHealth;

		charOneMPSlider.GetComponent<Slider>().maxValue = characterOne.GetComponent<Player>().maxMP;
		charOneMPSlider.GetComponent<Slider>().value = characterOne.GetComponent<Player>().currentMP;


		charOneMagicOption1.GetComponent<Text>().text = "Fire";
		charOneMagicOption2.GetComponent<Text>().text = "Ice";
		charOneMagicOption3.GetComponent<Text>().text = "Poison";

		charOneItemsOption1.GetComponent<Text>().text = "Potion";
		charOneItemsOption2.GetComponent<Text>().text = "Ether";

		charOneBattlePanel.SetActive(false);
		charOneMagicPanel.SetActive(false);
		//charOneSummonPanel.SetActive(false);
		charOneItemPanel.SetActive(false);


	}

	void setPlayerTwo(){

		charTwoName.GetComponent<Text>().text = characterTwo.GetComponent<Aeris>().characterName;
		charTwoCurrentHP.GetComponent<Text>().text = characterTwo.GetComponent<Aeris>().currentHealth.ToString();
		charTwoMaxHP.GetComponent<Text>().text = characterTwo.GetComponent<Aeris>().maxHealth.ToString();
		charTwoMP.GetComponent<Text>().text = characterTwo.GetComponent<Aeris>().currentMP.ToString();
		charTwoMBarrier.GetComponent<Slider>().value = characterTwo.GetComponent<Aeris>().mBarrierPercent;
		charTwoPBarrier.GetComponent<Slider>().value = characterTwo.GetComponent<Aeris>().pBarrierPercent;
		charTwoLimit.GetComponent<Slider>().value = characterTwo.GetComponent<Aeris>().limitPercent;

		charTwoHPSlider.GetComponent<Slider>().maxValue = characterTwo.GetComponent<Aeris>().maxHealth;
		charTwoHPSlider.GetComponent<Slider>().value = characterTwo.GetComponent<Aeris>().currentHealth;

		charTwoMPSlider.GetComponent<Slider>().maxValue = characterTwo.GetComponent<Aeris>().maxMP;
		charTwoMPSlider.GetComponent<Slider>().value = characterTwo.GetComponent<Aeris>().currentMP;


		charTwoMagicOption1.GetComponent<Text>().text = "Lightning";
		charTwoMagicOption2.GetComponent<Text>().text = "Heal";

		charTwoSummonOption1.GetComponent<Text>().text = "Shiva";

		charTwoItemsOption1.GetComponent<Text>().text = "Potion";
		charTwoItemsOption2.GetComponent<Text>().text = "Ether";

		charTwoBattlePanel.SetActive(false);
		charTwoMagicPanel.SetActive(false);
		charTwoSummonPanel.SetActive(false);
		charTwoItemPanel.SetActive(false);

	}


	/*void setPlayerThree(){

		charThreeName.GetComponent<Text>().text = "";
		charThreeCurrentHP.GetComponent<Text>().text = characterThree.GetComponent<Player>().currentHealth.ToString();
		charThreeMaxHP.GetComponent<Text>().text = characterThree.GetComponent<Player>().maxHealth.ToString();
		charThreeMP.GetComponent<Text>().text = characterThree.GetComponent<Player>().currentMP.ToString();
		charThreeMBarrier.GetComponent<Slider>().value = characterThree.GetComponent<Player>().mBarrierPercent;
		charThreePBarrier.GetComponent<Slider>().value = characterThree.GetComponent<Player>().pBarrierPercent;
		charThreeLimit.GetComponent<Slider>().value = characterThree.GetComponent<Player>().limitPercent;

		charThreeHPSlider.GetComponent<Slider>().maxValue = characterThree.GetComponent<Player>().maxHealth;
		charThreeHPSlider.GetComponent<Slider>().value = characterThree.GetComponent<Player>().currentHealth;

		charThreeMPSlider.GetComponent<Slider>().maxValue = characterThree.GetComponent<Player>().maxMP;
		charThreeMPSlider.GetComponent<Slider>().value = characterThree.GetComponent<Player>().currentMP;
		charThreeWait.GetComponent<Slider>().value = Random.Range(0.0f, 100.0f);

		charThreeOption1.GetComponent<Text>().text = "Attack";
		charThreeOption2.GetComponent<Text>().text = "Magic";
		charThreeOption3.GetComponent<Text>().text = "Summon";
		charThreeOption4.GetComponent<Text>().text = "Item";
	}*/



	public void charOneAttackButton(){
		Debug.Log("Char 1 ATTACK: I have been clicked!");
		charOneMagicPanel.SetActive(false);
		charOneItemPanel.SetActive(false);

	}
	public void charOneMagicButton(){
		Debug.Log("Char 1 MAGIC: I have been clicked!");
		charOneMagicPanel.SetActive(true);

		charOneItemPanel.SetActive(false);
	}
	public void charOneSummonButton(){
		Debug.Log("Char 1 SUMMON: I have been clicked!");
		charOneSummonPanel.SetActive(false);
	
	}
	public void charOneItemButton(){
		Debug.Log("Char 1 ITEM: I have been clicked!");
		charOneItemPanel.SetActive(true);
		charOneMagicPanel.SetActive(false);
	}

	public void charTwoAttackButton(){
		Debug.Log("ATTACK: I have been clicked!");
	}
	public void charTwoMagicButton(){
		Debug.Log("MAGIC: I have been clicked!");
		charTwoMagicPanel.SetActive(true);
		charTwoSummonPanel.SetActive(false);
		charTwoItemPanel.SetActive(false);
	}
	public void charTwoSummonButton(){
		Debug.Log("SUMMON: I have been clicked!");
		charTwoSummonPanel.SetActive(true);
		charTwoMagicPanel.SetActive(false);
		charTwoItemPanel.SetActive(false);
	}
	public void charTwoItemButton(){
		Debug.Log("ITEM: I have been clicked!");
		charTwoItemPanel.SetActive(true);
		charTwoMagicPanel.SetActive(false);
		charTwoSummonPanel.SetActive(false);
	}


	//Character 1 SubMenu Options
	public void char1MagicOption1()
	{
		Debug.Log("Magic Option1 is Clicked");
		charOneMagicPanel.SetActive(false);


	}

	public void char1MagicOption2()
	{
		Debug.Log("Magic Option2 is Clicked");
		charOneMagicPanel.SetActive(false);

	}

	public void char1MagicOption3()
	{
		Debug.Log("Magic Option3 is Clicked");
		charOneMagicPanel.SetActive(false);

	}
		
	public void char1ItemOption1()
	{
		Debug.Log("Item Option1 is Clicked");
		charOneItemPanel.SetActive(false);
	}

	public void char1ItemOption2()
	{
		Debug.Log("Item Option2 is Clicked");
		charOneItemPanel.SetActive(false);
	}

	//Character 2 SubMenu Options
	public void char2MagicOption1()
	{
		Debug.Log("Magic Option1 is Clicked");
		charTwoMagicPanel.SetActive(false);

	}

	public void char2MagicOption2()
	{
		Debug.Log("Magic Option2 is Clicked");
		charTwoMagicPanel.SetActive(false);

	}

	public void char2SummonOption1()
	{
		Debug.Log("Summon Option1 is Clicked");
		charTwoSummonPanel.SetActive(false);

	}

	public void char2ItemOption1()
	{
		Debug.Log("Item Option1 is Clicked");
		charTwoItemPanel.SetActive(false);

	}

	public void char2ItemOption2()
	{
		Debug.Log("Item Option2 is Clicked");
		charTwoItemPanel.SetActive(false);

	}
		


	public void EnemyClicked(Enemy enemy){
		BattleData data = bdList [currentPlayerIndex];
		data.target = enemy;
		currentPlayerIndex += 1;
	}

}
