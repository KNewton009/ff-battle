﻿using UnityEngine;
using System.Collections;

public class Enemy : Avatar {

	private GameObject battleController = GameObject.Find("Battle Controller"); 
	public bool isNPC = true;
	public Enemy() {
		currentHealth = maxHealth;
		maxHealth = 50;
		currentMP = maxMP;
		maxMP = 0;
		isNPC = true;
	}

	public enum Commands
	{
		Attack,
		Magic,
		Summon,
		Item
	};
	public enum Skills{
		Fire,
		Slow,
		Heal
	};
		

	/*bool death(int localCurrentHealth)
	{
		currentHealth = localCurrentHealth;
		if (localCurrentHealth <= 0) 
		{

			Destroy(this.gameObject); 
			return false;
		}
	}
*/
	bool setHealth(int h)
	{
		if (h <= 0) 
		{

			Destroy(this.gameObject); 
			return false;
		}


		if (h > this.maxHealth) 
		{
			this.currentHealth = this.maxHealth;
		}
		else 
		{
			this.currentHealth = h;
		}

		return true;

	}

	public bool modifyHealth(int howMuch)
	{
		return (this.setHealth(this.currentHealth + howMuch));

	}

	public int getHealth()
	{
		return this.currentHealth;
	}

	public int getMaxHealth()
	{
		return this.maxHealth;
	}


	void OnMouseDown ()
	{
		if (isNPC)
		{

			Camera.main.SendMessage ("EnemyClicked", this);

		}


	}

	}

